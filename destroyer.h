#ifndef DESTROYER
#define DESTROYER
struct destroy {
	struct destroy *previous;
	void (*destroyfunc)(void*);
	void *destroyable;
};
struct destroy *addDestroy(struct destroy *prev,void (*func)(void*),void *point) {
	struct destroy *a = (struct destroy*)malloc(sizeof(struct destroy));
	a->destroyfunc=func;a->destroyable=point;a->previous=prev;
	return a;
}
void destroyall(struct destroy *dest) {
	if(dest->previous!=NULL) destroyall(dest->previous);
	dest->destroyfunc(dest->destroyable);
	free(dest);
}
#endif
