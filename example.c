#include <stdio.h>
#include <stdlib.h>
#include "destroyer.h"
void bye(int *a) {
	free(a);
	printf("destroyed\n");
}
int main() {
	int *a = malloc(sizeof(int));
	struct destroy *d=addDestroy(NULL,(void (*)(void*))bye,(void*)a);
	int *b = malloc(sizeof(int));
	d=addDestroy(d,(void (*)(void*))bye,(void*)b);
	destroyall(d);
	return 0;
}
